# Signal Processing and Algorithmic Geometry Proseminar

## timeline

- 19 Oct: Intro
- 25 Oct: signal processing
  *Assignment1*
- 09 Nov: Audio processing / Neural networks
- 16 Nov: Neural Networks 
  *Assignment2*
- 23 Nov: Neural Networks for audio processing
- 03 Nov: *Deadline Assignment2*
